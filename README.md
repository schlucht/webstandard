# Standart Webvorlage #

Diese Vorlage kann zum Aufbau von einfachen Webseiten benutzt werden. Die Vorlage pasiert auf PostCSS und auf Typescript. Compiliert wird das ganze mit Parcel.<br><br>
[PostCSS](https://postcss.org/)<br>
[Typescript](https://www.typescriptlang.org/)<br>
[Parcel](https://parceljs.org/)

### Installation ###

```
   git clone https://schlucht@bitbucket.org/schlucht/webstandard.git newFoldername
   npm install 

```

### Betrieb ###

```
npm start
npm run build
```
Mit npm start wird von Parcel ein Server gestartet. http://localhost:1234
Mit npm run build werde die Dateien minimiert und komprimiert